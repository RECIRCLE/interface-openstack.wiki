# 1. Kliens által hívott abstract interface

## SnapshotManager

### Snapshot létrehozása (create)
Létrehoz egy új Snapshot-ot egy megadott id-ű volume-ból.

### Snapshot lekérdezése (get)
Visszaadja a megadott id alapján az snapshot-ot.

### Snapshot törlése (delete)
Törli a megadott id alapján az snapshot-ot.

### Snapshot-ok listázása (list)
Visszaadja az összes snapshot-ok.

## Snapshot
  * **id** (string) - A Snapshot azonosítója
  * **size** (integer) - A Snapshot mérete

# 2. OpenStackes megvalósítás