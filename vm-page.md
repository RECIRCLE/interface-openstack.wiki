# 1. Kliens által hívott abstract interface

## InstanceInterface

### Vm indítás

**Lehetőségek:**
* A portál elküldi az indítani kívánt gép RAM, CPU mag. prioritás értékeit. <br/>
 Előny: 
    - flexibilis erőforrás megadás, sok lehetőség <br/>
Hátrány: 
    - Pl. Openstacknél flavorok vannak, amiből sok kell, ha nincs olyan amilyen erőforrás kell, akkor újat kell csinálni


- A portálon is bevezetünk az openstackhez hasonló flavor rendszert, ami közül választhat a user.

**Előnyök:** 
* accounting egyszerűbben megvalósítható
* kevesebb variáció

**Hátrányok:** 
* nem biztos hogy minden cloud providernél van hasonló

**Döntés**

Bevezetjük a portálra is az előre megadott flavoroket, amiből majd lehet választani.

### create_vm_from_template

Meg kell adni: template id vagy név, erőforrás/flavor, új vm neve (legyen unique?)

Visszatérés: az új vm objektum

## Resources:

Az általunk használt fontos jellemzők a kellő adatokról. Egyfajta wrapperként működik a RECIRCLE protál és a felhő manager adatbázisa között. Ezek utaznak JSON formátumban az interfacenek küldött kérésekben és a válaszokban.

**Közös:** JSON() függvény amely json-ná alakítja.

### Instance
Virtuális gép adatai.

- id
- name
- falvor (a flavor id-je)
- image (az image/template id-je)
- disks (lemezek id-jei)
- status
- addresses
- launched_at
- terminated_at
- (ssh_keys)
- (console_url)

### Flavor
A flavorok adatai.

- id
- name
- ram
- vcpus
- initial_disk
- (extra_specs) (extra információk, még nincs benne)

### BlockDeviceMapping
Ha olyan gépet akarunk indítani ami nem egy templatet használ, meg kell adni neki egy vagy több bootolható eszközt. Ez az osztály tárolja erről az adatokat.

- boot_index
- uuid (A használandó erőforrás id-ja)
- source_type ( OpenStacknél: "image", "volume")
- volume_size
- destination_type (OpenStacknél: "image", "volume")
- delete_on_termination (bool)
- disk_bus (OpenStacknél: "scsi", "virtio", "sata", stb.)

# 2. OpenStackes megvalósítás

### create_base_vm
Template nélküli gép indítása. Meg kell adni a  bootolható eszközt. Előtte fel kell tölteni, vagy kiválasztani.

**Bemenet:** name, flavor (id), networks ( tömb, uuid kötelező), block_dev_map (általunk használt BlockDeviceMapping)

**Kimenet:** Instance objektum

### create_vm_from_template
Egy imageből csinál gépet.

**Bemenet:** name,image (id), favor (id), networks ( uuid-k tömbje)

**Kimenet:** A létrehozott gép remote_id-je.


### create_multiple_vm_from_template
Egy imageből csinál több gépet.

**Bemenet:** name, image (id), favor (id), networks ( uuid-k tömbje), number 

**Kimenet:** A létrehoztt gépek remote_id-jei egy tömbben.


### get_vm
Lekéri a név vagy id alapján a vm objektumot az OpenStack-től. Kivételt dob, ha nincs megadva valid id vagy név.

**Bemenet:** instance_id

**Kimenet:** Instance objektum

### list_all_vm
Listázza az összes virtuális gépet.

**Bemenet:** -
**Kimenet:** Instance tömb

### Alapvető funkciók:
Alapvető operációk meghívva az openstackes vm-en.

**Támogatott:** start_vm, stop_vm, reboot_vm *(soft)*, reset_vm *(hard)*, destroy_vm, suspend_vm, wake_up_vm

**Bemenet:** name or id, rebootnál reboot_type ("soft", "hard")

**Kimenet:** None

### create_flavor
Létrehoz egy új flavort. A névnek egyedinek kell lenni. 

**Bemenet:** name, ram, vcpus, inital_disk

**Kimenet:** Flavor objektum


### get_flavor
Visszaadja a keresett flavort. 

**Bemenet:** flavor_id

**Kimenet:** Flavor objektum


### list_flavors

**Bemenet:** -

**Kimenet:** Flavor objektumok tömbje


### delete_flavor
Törli a megadott flavort. 

**Bemenet:** flavor_id

**Kimenet:**  - 

### get_vnc_console
Lekérdezi és visszadja egy vm-hez tartozó konzol url-jét

**Bemenet:** server_id
**Kimenet:** VNC konzol URL-je

### resize_vm
Átméretezési kérést indít el az OpenStackben. Ezt el kell fogadnia egy adminisztrátornak, hogy ténylegesen új erőforrások legyenek. Csak leállított állapotban lehet végrehajtani.

**Bemenet:** server_id
**Kimenet:** resize_request objektum

### accept_resize

### attach_volume
A megadott lemezt hozzá csatolja a megadott géphez. Meg lehet adni a mountolási helyet, és a kívánt disk id-jét. Egy volume_attachment ID-ját adja vissza, ennek a segítségével lehet lecsatolni a lemezt.

**Bemenet:** server_id, disk_id, (mount_point)
**Kimenet:** volume_attachment ID

### detach_volume
Egy volume attachmentet lehet törölni vele, vagyis a megadott gépről lecsatolja a lemezt. a lemez maga nem törlődik.

**Bemenet:** volume_attchment_id
**Kimenet:** None

### convert_server_to_instance
Segédfüggvény. Az OpenStack-es szervert konvertálja át Instance objektummá.

**Bemenet:** server
**Kimenet:** Instance objektum
