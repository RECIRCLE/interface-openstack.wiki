# 1. Kliens által hívott abstract interface

## ImageManager

### Image létrehozása / feltöltése (create)
Létrehoz egy új Image-et a megfelelő paraméterekkel.

### Image lekérdezése (get)
Visszaadja a megadott id alapján az image-et.

### Image törlése (delete)
Törli a megadott id alapján az image-et.

### Image-ek listázása (list)
Visszaadja az összes image-et.

## Image
  * **id** (string) - Az Image azonosítója
  * **name** (string) - Az Image neve
  * **format** (string/enum) - Az Image formátuma (ami, ari, aki, vhd, vmdk, raw, qcow2, vdi, iso)

# 2. OpenStackes megvalósítás