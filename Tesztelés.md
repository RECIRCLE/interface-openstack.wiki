# Az interfész tesztelése

## Unit tesztek

A tesztelés során a csak a saját kódrészeket kell vizsgálni. Ezért a külső kapcsolatokat mock-olni kell. A mockoláshoz a Python beépített mock libraryjét használjuk. Érdemes az egész kapcsolatot elfedni egy patcheléssel.
Bővebb információ: [Patch in Python](https://docs.python.org/3/library/unittest.mock.html#patch).
Ezután a kapott Mock objektumnak felülírni a szükséges függvényeit.

Péda az OpenStackes implementációból:

``` python
   @patch('openstack.connect')
    def setUp(self, mock_connect):
        self.compute = MagicMock()
        self.compute.get_server = MagicMock(return_value=servers[0])
        mock_connect().compute = self.compute
        self.manager = OSVirtualMachineManager(auth=fake_auth)
```

A felhőmenedzser által visszaadott objektumok helyett is mockolt példányokat használunk. Ezeket lehetőleg a tesztfájl elején sorold fel, vagy ha sok van akkor külön fájlban. Egyszerre egy funkciót tesztelj, pl.: meghívódott-e és jó paraméterekkel a függvény, hibát kellet dobjon a kód, jó objektumot adott vissza, ... . Igyekezz kimerítő teszteket írni különféle paraméterezéssel, a kritikusabb részekre fókuszálva.

Tapasztalt hibák:
- A patch által visszaadott objektumon elfelejtett "()", ha az meg van hívva a tesztelni kívánt kódban.
- Nem futnak le a tesztek: A gyökérkönyvtárból kell meghívni a teszteket, mert abszolút importokat használunk, mivel egy külön package-ként szeretnénk használni.
