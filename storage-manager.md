# 1. Kliens által hívott abstract interface

## StorageManager

### Volume létrehozása (create)
Létrehoz egy új volume-ot a megfelelő paraméterekkel.

### Volume lekérdezése (get)
Visszaadja a megadott id alapján az volume-ot.

### Volume törlése (delete)
Törli a megadott id alapján az volume-ot.

### Volume-ok listázása (list)
Visszaadja az összes volume-ot.

## Volume
  * **id** (string) - A Volume azonosítója
  * **size** (integer) - A Volume mérete (GByte-ban)
  * **bootable** (boolean) - A Volume bootolhatósága

# 2. OpenStackes megvalósítás